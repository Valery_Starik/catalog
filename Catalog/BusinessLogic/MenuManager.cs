﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public class MenuManager
    {
        public static ItemCollection < ItemDisk > Menu(ItemCollection < ItemDisk > DA)
        {
            ItemCollection < ItemDisk > DiskArray = DA;
            Keyboard keyboard = new Keyboard();
            string s = "0";
            string StatusUser = ClientConsole.ClientConsole.Status();
            keyboard.EventPressKey += MyEvents.PressKey_Handler;
            DiskArray = BusinessLogic.ChooseLoad(DiskArray);
            while (s != "7")
            {
                s = ClientConsole.ClientConsole.MainMenuLegend();
                keyboard.PressKeyEvent(DiskArray, StatusUser, s);
            }
            return DiskArray;
        }
    }

}
