﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{

    public delegate void PressKeyEventHandler(ItemCollection < ItemDisk > DiskArray, string StatusUser, string UserPressKey);

    public class Keyboard
    {
        public event PressKeyEventHandler EventPressKey = null;
        public event PressKeyEventHandler EventCollect = null;
        
        public void PressKeyEvent(ItemCollection < ItemDisk > DiskArray, string StatusUser, string UserPressKey)
        {
            if (EventPressKey != null)
            {
                EventPressKey.Invoke(DiskArray, StatusUser, UserPressKey);
            }
        }
        public void CollectEvent(ItemCollection < ItemDisk > DiskArray, string StatusUser, string UserPressKey)
        {
            if (EventCollect != null)
            {
                EventCollect.Invoke(DiskArray, StatusUser, UserPressKey);
            }
        }
    }

    public static class MyEvents
    {
        public static void PressKey_Handler(ItemCollection < ItemDisk > DiskArray, string StatusUser, string UserPressKey)
        {
            Keyboard keyboard = new Keyboard();
            keyboard.EventCollect += EventCollect_Handler;
            switch (UserPressKey)
            {
                case "1":
                    BusinessLogic.View(DiskArray);
                    break;
                case "2":
                case "3":
                case "4":
                    keyboard.CollectEvent(DiskArray, StatusUser, UserPressKey);
                    break;
                case "5":
                    int i = BusinessLogic.Find_perf(ClientConsole.ClientConsole.ConsoleFind(), DiskArray);
                    ClientConsole.ClientConsole.ConsoleClear();
                    ClientConsole.ClientConsole.WriteConsole(DiskArray[i]);
                    break;
                case "6":
                    Serialization.Save(DiskArray);
                    Serialization.WriteXMLSerial(DiskArray);
                    break;
                case "7":
                     break;
                default:
                     ClientConsole.ClientConsole.Correct();
                    break;
            }
        }

        public static void EventCollect_Handler(ItemCollection<ItemDisk> DiskArray, string StatusUser,
            string UserPressKey)
        {
            if (StatusUser.Equals("Admin", StringComparison.OrdinalIgnoreCase))
            {
                switch (UserPressKey)
                {
                    case "2":
                        BusinessLogic.AddDisk(ClientConsole.ClientConsole.ConsoleAddDisk(), DiskArray);
                        break;
                    case "3":
                        BusinessLogic.Delete(ClientConsole.ClientConsole.ConsoleDelete(), DiskArray);
                        break;
                    case "4":
                        BusinessLogic.SortBy(DiskArray);
                        break;
                }
                BusinessLogic.View(DiskArray);
            }
            else ClientConsole.ClientConsole.Security();
        }
        }
}
