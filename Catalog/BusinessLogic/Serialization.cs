﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace BusinessLogic
{
   public static class Serialization
    {
        public static void Save(object DiskArray)
        {
            FileStream fs = new FileStream("myDisks.dat", FileMode.Create, FileAccess.Write);

            BinaryFormatter formatter = new BinaryFormatter();
            formatter.Serialize(fs, DiskArray);
            fs.Close();
        }


        public static object Load()
        {
            FileStream fs = new FileStream("myDisks.dat", FileMode.Open);

            BinaryFormatter formatter = new BinaryFormatter();
            var result = formatter.Deserialize(fs);
            fs.Close();
            return result;
        }

        public static void WriteXMLSerial(ItemCollection < ItemDisk > DiskArray)
        {
            XmlSerializer x = new XmlSerializer(typeof(ItemCollection < ItemDisk >));
            TextWriter writer = new StreamWriter("myDisks.xml");
            x.Serialize(writer, DiskArray);
            writer.Close();
        }

        public static ItemCollection < ItemDisk > ReadXMLSerial()
        {
            XmlSerializer x = new XmlSerializer(typeof(ItemCollection < ItemDisk >));

            FileStream fs = new FileStream("myDisks.xml", FileMode.Open);
            ItemCollection < ItemDisk > DiskArray;
            DiskArray = (ItemCollection < ItemDisk >)x.Deserialize(fs);
            fs.Close();
            return DiskArray;
        }
    }
}
