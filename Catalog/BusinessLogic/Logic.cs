﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClientConsole;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using NUnit.Framework;

namespace BusinessLogic
{
    [Serializable]
    public class Item : IComparable
    {
        public Item()
        {

        }

        public Item(string perf, string tit, string releas, string not, string gen)
        {
            this.performer = perf;
            this.title = tit;
            this.released = releas;
            this.number_of_tracks = not;
            this.gentre = gen;
        }

        private string performer;
        public string Performer
        {
            get { return performer; }
            set { performer = value; }
        }
        private string title;
        public string Title
        {
            get { return title; }
            set { title = value; }
        }
        private string released;
        public string Released
        {
            get { return released; }
            set { released = value; }
        }
        private string gentre;
        public string Gentre
        {
            get { return gentre; }
            set { gentre = value; }
        }
        private string number_of_tracks;
        public string Number_of_tracks
        {
            get { return number_of_tracks; }
            set { number_of_tracks = value; }
        }

        public override string ToString()
        {
            return performer + " " + title + " " + released + " " + number_of_tracks + " " +
                                    gentre; ;
        }

        public int CompareTo(object obj)
        {
            Item other = obj as Item;
            return this.performer.CompareTo(other.performer);
        }

        public ItemDisk[] DiskArray;

    }
    [Serializable]
    public class ItemDisk : Item
    {
        public ItemDisk(string perf, string tit, string releas, string not, string gen) : base(perf, tit, releas, not, gen)
        {
        }

        public ItemDisk()
        {
            
        }
    }

    [Serializable]
    public class ItemCollection<TItem> : List<TItem> where TItem : Item
    {
        public ItemCollection()
        {
        }
        public static List<TItem> DiskArray = new List<TItem>();
    }
    public class BusinessLogic
    {
        public static ItemCollection<ItemDisk> AddDisk(string str, ItemCollection < ItemDisk > DiskArray)
        {
            try
            {
                string[] split = str.Split(new Char[] {' '});
                string a = split[0];
                string c = split[2];
                string b = split[1];
                string d = split[3];
                string e = split[4];
                ItemDisk res = new ItemDisk(a, b, c, d, e);
                DiskArray.Add(res);
            }
            catch (Exception)
            {
                throw new Exception("Error format");
            }
            return DiskArray;
        }
        public static ItemCollection<ItemDisk> ChooseLoad(ItemCollection<ItemDisk> DiskArray)
        {
            switch (ClientConsole.ClientConsole.ConsoleLoad())
            {
                case "1":
                    {
                        DiskArray = (ItemCollection<ItemDisk>)Serialization.Load();
                    }
                    break;
                case "2":
                    {
                        DiskArray = Serialization.ReadXMLSerial();
                    }
                    break;
                default:
                    {
                    }
                    break;
            }
            return DiskArray;
        }
        public static void Delete(int index, ItemCollection < ItemDisk > MyDisks)
        {
            ItemCollection < ItemDisk > DiskArray = MyDisks;
            DiskArray.RemoveAt(index);
        }

        public static int Find_perf(string str, ItemCollection < ItemDisk > MyDisks)
        {
            ItemCollection < ItemDisk > DiskArray = MyDisks;
            int index = DiskArray.IndexOf(DiskArray.Find(item => item.Performer == str));
            return index;
        }

        public static void SortBy(ItemCollection < ItemDisk > MyDisks)
        {
            ItemCollection < ItemDisk > DiskArray = MyDisks;

            switch (ClientConsole.ClientConsole.ConsoleSort())
            {
                case "1":
                    {
                        DiskArray.Sort(((c1, c2) => c1.Performer.CompareTo(c2.Performer)));
                    }
                    break;
                case "2":
                    {
                        DiskArray.Sort(((c1, c2) => c1.Title.CompareTo(c2.Title)));
                    }
                    break;
                case "3":
                    {
                        DiskArray.Sort(((c1, c2) => c1.Released.CompareTo(c2.Released)));
                    }
                    break;
                case "4":
                    {
                        DiskArray.Sort(((c1, c2) => c1.Number_of_tracks.CompareTo(c2.Number_of_tracks)));
                    }
                    break;
                case "5":
                    {
                        DiskArray.Sort(((c1, c2) => c1.Gentre.CompareTo(c2.Gentre)));
                    }
                    break;
                default:
                    {
                        DiskArray.Sort();
                    }
                    break;
            }

            //MyDisks = (ItemCollection<ItemDisk>) DiskArray.Sort(ClientConsole.ClientConsole.ConsoleSort());
            //return MyDisks;
        }

        public static void View(ItemCollection < ItemDisk > MyDisks)
        {
            ItemCollection < ItemDisk > DiskArray = MyDisks;
            ClientConsole.ClientConsole.ConsoleClear();
            ClientConsole.ClientConsole.ConsoleViewHeader();
            foreach (ItemDisk p in DiskArray)
            {
                string ind = Convert.ToString(DiskArray.IndexOf(p));
                string[] Data = new string[] {ind, p.Performer, p.Title,
                p.Released, p.Number_of_tracks, p.Gentre};
                ClientConsole.ClientConsole.ConsoleView(Data);
            }

        }

        public static ItemCollection < ItemDisk > CreateCollection()
        {
            ItemCollection < ItemDisk > DiskArray = new ItemCollection < ItemDisk >();

            return DiskArray;
        }
    }

    
}
