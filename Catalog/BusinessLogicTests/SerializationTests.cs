﻿using BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace BusinessLogic.Tests
{
    [TestFixture]
    public class SerializationTests
    {

        static ItemCollection < ItemDisk > DiskArrayTest = new ItemCollection < ItemDisk >();


        [SetUp]
        public static void SetUpTestsMethod()
        {
            DiskArrayTest = new ItemCollection < ItemDisk >();
            ItemDisk DiskList = new ItemDisk("OE", "Supesemetriya", "2004", "12", "Rock");
            DiskArrayTest.Add(DiskList);
        }

        [Test]
        public void SaveTest()
        {
            Serialization.Save(DiskArrayTest);
            object a = Serialization.Load();
            Assert.AreEqual(Convert.ToString(DiskArrayTest), Convert.ToString(a));
        }

        [Test]
        public void LoadTest()
        {
            Serialization.Save(DiskArrayTest);
            object a = Serialization.Load();
            Assert.AreEqual(Convert.ToString(a), Convert.ToString(DiskArrayTest));
        }

        [Test]
        public void WriteXMLSerialTest()
        {
            Serialization.WriteXMLSerial(DiskArrayTest);
            object a = Serialization.ReadXMLSerial();
            Assert.AreEqual(Convert.ToString(DiskArrayTest), Convert.ToString(a));
        }

        [Test]
        public void ReadXMLSerialTest()
        {
            Serialization.WriteXMLSerial(DiskArrayTest);
            object a = Serialization.ReadXMLSerial();
            Assert.AreEqual(Convert.ToString(a), Convert.ToString(DiskArrayTest));
        }

        [TearDown]
        public static void TearDownTestMethod()
        {
            DiskArrayTest = null;
        }
    }
}