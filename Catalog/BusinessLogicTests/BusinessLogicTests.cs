﻿//using Microsoft.VisualStudio.TestTools.UnitTesting;
using BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace BusinessLogicTests
{
    [TestFixture]
    public class BusinessLogicTests
    {
        static ItemCollection < ItemDisk > DiskArrayTest = new ItemCollection < ItemDisk >();
        
        [SetUp]
        public static void SetUpTestsMethod()
        {
            DiskArrayTest = new ItemCollection < ItemDisk >();
            ItemDisk DiskList = new ItemDisk("OE", "Supesemetriya", "2004", "12", "Rock");
            DiskArrayTest.Add(DiskList);
        }


        [Test]
        public void AddDiskTest()
        {
            BusinessLogic.BusinessLogic.AddDisk("s s s s s", DiskArrayTest);
            Assert.AreEqual(2, DiskArrayTest.Count);
        }

        [Test]
        public void DeleteTest()
        {
            
            BusinessLogic.BusinessLogic.Delete(0, DiskArrayTest);
            Assert.AreEqual(0, DiskArrayTest.Count);
        }

        [Test]
        public void Find_perfTest()
        {
            int res = Convert.ToInt16(BusinessLogic.BusinessLogic.Find_perf("OE", DiskArrayTest));
            Assert.AreEqual(0, res);
        }

        [Test]
        public void CreateCollectionTest()
        {
            object a = BusinessLogic.BusinessLogic.CreateCollection();
            ItemCollection < ItemDisk > DA = new ItemCollection < ItemDisk >();
            Assert.AreEqual(DA, a);
        }
        [TearDown]
        public static void TearDownTestMethod()
        {
            DiskArrayTest = null;
        }
    }
}