﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientConsole
{
    public static class ClientConsole
    {
        public static string Status()
        {
            string UserStatus = "";
            while ((UserStatus != "Admin")&(UserStatus != "User")) 
               // (!UserStatus.Equals("User", StringComparison.OrdinalIgnoreCase)))
            {
                    Console.WriteLine(Resource.CorrectUser);
                    UserStatus = Console.ReadLine();
            }
            return UserStatus;
        }

        public static void Security()
        {
            Console.WriteLine(Resource.Security);
        }

        public static string MainMenuLegend()
        {
            Console.ForegroundColor = ConsoleColor.Black;
            Console.BackgroundColor = ConsoleColor.White;
            Console.WriteLine(Resource.MenuLegend);
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.White;
            return Console.ReadLine();
        }

        public static void Correct()
        {
             Console.WriteLine(Resource.Correct);
        }

        //public static void ConsoleView(string str)
        //{
        //    Console.BackgroundColor = ConsoleColor.Black;
        //    Console.ForegroundColor = ConsoleColor.Green;
        //    System.Console.WriteLine(str);
        //    Console.BackgroundColor = ConsoleColor.Black;
        //    Console.ForegroundColor = ConsoleColor.White;
        //}
        public static void ConsoleViewHeader()
        {
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.Green;
            string[] headerColumns = new string[] {Resource.Index, Resource.Performer, Resource.Title,
                Resource.Release, Resource.Tracks, Resource.Genre};
            Console.WriteLine("{0,-8} {1,-10} {2,10} {3,-10} {4,-9} {5,-15}\n", headerColumns);
        }

        public static void ConsoleView(string[] Data)
        {
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.White;
            //System.Console.WriteLine(str);
            Console.WriteLine("{0,-8} {1,-10} {2,-10} {3,-10} {4,-9} {5,-15}", Data);
        }

        public static void ConsoleClear()
        {
            Console.Clear();
        }

        public static int ConsoleDelete()
        {
            Console.WriteLine(Resource.Choise_delete);
            return Convert.ToInt32(Console.ReadLine());
        }

        public static string ConsoleFind()
        {
            Console.WriteLine(Resource.SearchPerformer);
            return Console.ReadLine();
        }

        public static void WriteConsole(object s)
        {
            Console.WriteLine(s);
        }

        public static string ConsoleSort()
        {
            Console.WriteLine(Resource.Choise_sort);
            return Console.ReadLine();
        }

        public static string ConsoleLoad()
        {
            Console.WriteLine(Resource.Load);
            return Console.ReadLine();
        }

        public static string ConsoleAddDisk()
        {
            try
            {
                Console.WriteLine(Resource.Add);
                return Console.ReadLine();
            }
            catch (Exception)
            {
                Console.WriteLine(Resource.Exception);
                throw;
            }
        }

    }
}
